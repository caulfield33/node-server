
const gulp   = require('gulp'),
      server = require('gulp-server-livereload'),
      watch  = require('gulp-watch'),
      shell = require('gulp-shell');


gulp.task('live', function() {
    gulp.src('private')
        .pipe(server({
            livereload: true,
            defaultFile: 'index.html',
            open: false
        }));
});

gulp.task('watch', function() {
    gulp.watch('private/js/*.js');
    gulp.watch('private/css/*.css');
    gulp.watch('private/index.html')
});

gulp.task('server', shell.task('node server'));

gulp.task('default', ['server' ,'watch', 'live']);
