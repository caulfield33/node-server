const express       = require('express');
const morgan        = require('morgan');
const bodyParser    = require('body-parser');
const mongoose      = require('mongoose');
const shared        = require('./env/shared');
const productRoutes = require('./api/routes/products');
const orderRoutes   = require('./api/routes/orders');
const commentRoutes = require('./api/routes/comments');
const userRoutes    = require('./api/routes/users');
const statisticsRoutes    = require('./api/routes/statistics');

const app           = express();

mongoose.connect(shared.mongodbConnection);

mongoose.Promise = global.Promise;


//console log request info
app.use(morgan('dev'));

//app utility
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({extended: false}));

//response headers
app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", shared.accessControl);
    response.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE OPTIONS");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Z-Key");

    if (request.method === "OPTIONS") {
        response.header("Access-Control-Allow-Origin", shared.accessControl);
        response.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
        response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Z-Key");
        return response.status(200).json({})
    }

    next();
});

//routes
app.use('/products',   productRoutes);
app.use('/orders',     orderRoutes);
app.use('/comments',   commentRoutes);
app.use('/users',      userRoutes);
app.use('/statistics', statisticsRoutes);

//Sent private
app.use(express.static('private'));

//error 404 caching
app.use((request, response, next) => {
    const error = new Error('Some thing was crash');
    error.status = 404;
    next(error);
});
app.use((error, request, response) => {
    response.status(error.status || 500);
    response.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;
