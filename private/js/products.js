window.onload = function () {
    getProducts();
};

function createProduct() {

    let newProduct = {
        "name"          : $("#nameProduct").val(),
        "description"   : $("#descriptionProduct").val(),
        "price"         : $("#priceProduct").val(),
        "owner"         : $("#ownerProduct").val(),
        "available"     : $("#availableProduct").val(),
        "images"        : $('#productImage').val(),
        "discount"      : $("#discountProduct").val(),
        "type"          : $("#typeProduct").val(),
        "category"      : $("#categoryProduct").val(),
        "vipProduct"    : $("#vipProduct").val(),
        "maxOrder"      : $("#maxOrderProduct").val(),
        "infoLinks"     : "https://twitter.com/",
    };

    $.ajax({
        url: `${baseUrl}/products`,
        contentType: 'application/json',
        crossDomain: true,
        data: JSON.stringify(newProduct),
        type: 'POST',
        success: function () {
            dialogAlert("Element was created", "success");
            getProducts();
            updateModal();

        },
        error: function (request, status, err) {
            dialogAlert(request.responseJSON.msg, "danger");
        }
    });

}

function rebuildForm() {

    $('#productForm').trigger('reset');
    $('#updateModalLabel').html('Create record');
    $('#modalConfirmBtn').html("Create");
    $('#modalConfirmBtn').attr("data-target", "#createConfirmModal");

}

function getProducts() {
    closeDetail();

    $(".cssload-container").show();
    $("#orderNav").removeClass('active');
    $("#productNav").addClass('active');

    $.ajax({
        url: `${baseUrl}/products`,
        dataType: 'json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (response) {
            $(".cssload-container").fadeOut('slow');
            productsAppend(response.products);
            products = response.products;
        },
        error: function (request, status, error) {
            $(".dialogAlert").fadeOut("slow");
            dialogAlert(request.responseJSON.msg, "danger");
            emptyPage("No products to display")
        }
    });
}

function updateProduct() {
    event.preventDefault();

    let updatedData = [
        {"propName": "name", "value": $("#nameProduct").val()},
        {"propName": "owner", "value": $("#ownerProduct").val()},
        {"propName": "description", "value": $("#descriptionProduct").val()},
        {"propName": "type", "value": $("#typeProduct").val()},
        {"propName": "category", "value": $("#categoryProduct").val()},
        {"propName": "price", "value": $("#priceProduct").val()},
        {"propName": "discount", "value": $("#discountProduct").val()},
        {"propName": "maxOrder", "value": $("#maxOrderProduct").val()},
        {"propName": "available", "value": $("#availableProduct").val()},
        {"propName": "vipProduct", "value": $("#vipProduct").val()},
    ];

    $.ajax({
        url: `${baseUrl}/products/${products[productID]._id}`,
        contentType: 'application/json',
        crossDomain: true,
        data: JSON.stringify(updatedData),
        type: 'PUT',
        success: function () {
            dialogAlert("Element was updated.", "success");
            getProducts();
            updateModal();
        },
        error: function (request, status, error) {
            console.log(request);
            dialogAlert(`Something went wrong. ${error}`, "danger");
        }
    });
}

function updateProductConfirm(ID) {
    $('#modalConfirmBtn').html("Update");
    $('#modalConfirmBtn').attr("data-target", "#updateConfirmModal");

    productID = ID.getAttribute('productID');
    $("#nameProduct").val(products[productID].name);
    $("#ownerProduct").val(products[productID].owner);
    $("#descriptionProduct").val(products[productID].description);
    $("#typeProduct").val(products[productID].type);
    $("#categoryProduct").val(products[productID].category);
    $("#priceProduct").val(products[productID].price);
    $("#discountProduct").val(products[productID].discount);
    $("#maxOrderProduct").val(products[productID].maxOrder);
    $("#availableProduct").val(products[productID].available);
    $("#vipProduct option").prop('selected', !products[productID].vipProduct)

}

function deleteProduct() {
    $.ajax({
        url: `${baseUrl}/products/${deletingID}`,
        dataType: 'json',
        crossDomain: true,
        headers: {"Access-Control-Allow-Origin": "*"},
        type: 'DELETE',
        success: function () {
            getProducts();
            dialogAlert("Element was deleted.", "success");
            closeDetail()
        },
        error: function (request, status, error) {
            dialogAlert(`Something went wrong. ${error}`, "danger");
        }
    });
}

function deleteProductConfirm(ID) {
    deletingID = ID.getAttribute('productID')
}

function productsAppend(products) {
    let index = 1;
    $('.centerPage').empty();
    $('.centerPage').append(
        "<table class=\"table centralTable\" style=\"margin-bottom: 0\">\n" +
        "        <thead class=\"thead-dark\">\n" +
        "\n" +
        "        </thead>\n" +
        "        <tbody>\n" +
        "\n" +
        "        </tbody>\n" +
        "      </table>")
    $('.centralTable > tbody').hide().fadeIn('slow');
    $('.centralTable > thead').empty().append("<tr>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Index</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Name</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Price</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Owner</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Available</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">More Info</th>\n" +
        "                </tr>");
    $(".centralTable > tbody").empty();
    for (let product of products) {
        $(".centralTable > tbody").append("" +
            "<tr>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + index + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.name + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.price + " $</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.owner + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.available + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'> <button style='display: block;margin: 0 auto; width: 100%' type='button' class='btn btn-outline-info' onclick='moreData(" + (index - 1) + ",true)'><i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\n</button></td>" +
            "</tr>");
        index++
    }

}

function moreData(i, controll) {

    $('#updateModalLabel').html('Update record');
    $('#modalConfirmBtn').html("Update");
    $("#info").empty().append("" +
        "<div class=\"card\">\n" +
        "<a href=\"#\" style='text-align: right' onclick='closeDetail()' class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
        "                    <img class=\"card-img-top\" src=\"" + products[i].images + "\" alt=\"Card image cap\">\n" +
        "                    <div class=\"card-body\">\n" +
        "                        <h5 class=\"card-title\">" + products[i].name + "</h5>\n" +
        "                        <p class=\"font-weight-light\">Owner: " + products[i].owner + "</p>\n" +
        "                        <p class=\"card-text\">" + products[i].description + "</p>\n" +
        "                        <ul class=\"list-group infoUl\">\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Created : <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].createdDate.split(" ", 6).join(" ") + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Discount: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].discount + " %</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Category: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].category + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Available: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].available + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Max Order: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].maxOrder + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Price: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].price + " $</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Views: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].views + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Wish: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].wish + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Vip Product: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i].vipProduct + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">ID: <span class='badge badge-primary badge-pill' style='float: right'>" + products[i]._id + "</span></li>\n" +
        "                        </ul>" +
        " <div style='margin-top: 10px' class=\"btn-group btn-group-justified\" role=\"group\" aria-label=\"Justified button group\">\n" +
        "   <a data-toggle=\"modal\" data-target=\"#updateModal\" productID=\"" + i + "\" class=\"btn btn-success\" href=\"#\" role=\"button\" onclick='updateProductConfirm(this)'>Update</a>\n" +
        "   <a  data-toggle=\"modal\" data-target=\"#deleteModal\" productID=\"" + products[i]._id + "\" class=\"btn btn-danger\" href=\"#\" role=\"button\" onclick='deleteProductConfirm(this)'>Delete</a>\n" +
        " </div>" +
        "                    </div>\n" +
        "                </div>").hide().fadeIn(500);


    if (controll) {
        $(".btn-group").show()

    } else {
        $(".btn-group").hide()

    }


}
