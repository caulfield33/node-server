
function getOrders() {

    closeDetail();
    $(".cssload-container").show();
    $("#productNav").removeClass('active');
    $("#orderNav").addClass('active');

    $.ajax({
        url: `${baseUrl}/orders`,
        dataType: 'json',
        success: function (response) {
            appendOrders(response.orders);
            orders = response.orders;
        },
        error: function (request, status, err) {

            if(request.responseJSON.msg === "Cant find any orders"){
                $(".dialogAlert").fadeOut("slow");
                dialogAlert(request.responseJSON.msg, "danger");
                emptyPage("No order to display.")

            } else {
                dialogAlert(err, "danger")
            }

        }
    }).then(function () {
        $(".cssload-container").fadeOut("slow");
    })
}

function appendOrders(orders) {
    $('.centerPage').empty();
    $('.centerPage').append(
        "<table class=\"table centralTable\" style=\"margin-bottom: 0\">\n" +
        "        <thead class=\"thead-dark\">\n" +
        "\n" +
        "        </thead>\n" +
        "        <tbody>\n" +
        "\n" +
        "        </tbody>\n" +
        "      </table>")

    let index = 1;
    $('.centralTable > table').hide().fadeIn('slow');
    $('.centralTable > thead').empty().append("<tr >\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Index</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Order ID</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Type</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Courier</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Pay status</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Products</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Order info</th>\n" +
        "                </tr>");
    $(".centralTable > tbody").empty();
    for (let order of orders) {
        $(".centralTable  >tbody").first().append("" +
        "<tr style='border-bottom: 1px solid #cbcbcb'>" +
        "<td style='border-right: 1px solid #cbcbcb'>" + index + "</td>" +
        "<td style='border-right: 1px solid #cbcbcb'>" + order._id + "</td>" +
        "<td style='border-right: 1px solid #cbcbcb'>" + order.type + "</td>" +
        "<td style='border-right: 1px solid #cbcbcb'>" + order.courier + "</td>" +
        "<td style='border-right: 1px solid #cbcbcb'>" + order.payed + "</td>" +
        "<td style='border-right: 1px solid #cbcbcb'><button style='display: block;margin: 0 auto; width: 100%' type=\"button\" class='btn btn-outline-info' data-toggle=\"collapse\" data-target=\"#productList" + index + "\" aria-expanded=\"false\" aria-controls=\"collapseExample\" >" + order.products.length + "</button></td>" +
        "<td style='border-right: 1px solid #cbcbcb'> <button style='display: block;margin: 0 auto; width: 100%' type='button' class='btn btn-outline-info' onclick='orderMoreInfo("+ (index - 1) +")'>\+</button></td>" +
        "</tr>" +
        "<tr style='background-color: lightblue'>" +
        "<td colspan=\"7\" style='padding: 0; border: none'>" +
        "<div class=\"collapse\" id=\"productList" + index + "\">\n" +
        "<table style='margin:10px;border: 1px solid #cbcbcb; border-top: none; width: calc(100% - 20px);background-color: white'>" +
        "<thead>" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Index</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Quantity</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Name</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Price</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Owner</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Available</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">More Info</th>\n" +
        "</thead>" +
        "<tbody class='"+ order._id  + "'>" +
        "</tbody>" +
        "</table>" +
        "</div>" +
        "</td>" +
        "</tr>" +
            "            <hr style=\"margin-top: 0\">"
    );
        let i = 0;

        for(let product of order.products){

            if (product._id === null){
                $(`.${order._id}`).append("" +
                    "<tr>" +
                    "<td style='border-right: 1px solid #cbcbcb' colspan=\"1\">" + index +
                    "</td>" +
                    "<td colspan=\"6\">" +
                    "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    "  <strong>Product doesent exist enymore</strong>" +
                    "</div></td>" +
                    "</tr>");
            } else {
                $(`.${order._id}`).append("" +
                    "<tr>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + (i + 1) + "</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + product.quantity + "</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + product._id.name + "</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + product._id.price + " $</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + product._id.owner + "</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'>" + product._id.available + "</td>" +
                    "<td style='border-right: 1px solid #cbcbcb'> <button style='display: block;margin: 0 auto; width: 100%' type='button' class='btn btn-outline-info' onclick='moreData(" + (index - 1) + ",false)'><i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\n</button></td>" +
                    "</tr>");
            }


            i++
        }
        index++
    }

}

function orderMoreInfo(i) {
    console.log(orders)
    $('#updateModalLabel').html('Update record');
    $('#modalConfirmBtn').html("Update");
    $("#info").empty().append("" +
        "<div class=\"card\">\n" +
        "<a href=\"#\" style='text-align: right' onclick='closeDetail()' class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
        "                    <div class=\"card-body\">\n" +
        "                        <ul class=\"list-group infoUl\">\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Delivery time: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].deliveryTime.split(" ", 6).join(" ") + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Description: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].description + " %</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Comment: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].comment + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Type: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].type + "</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Delivery price: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].deliveryPrice + " $</span></li>\n" +
        "                            <li class=\"list-group-item list-group-item-action\">Courier: <span class='badge badge-primary badge-pill' style='float: right'>" + orders[i].courier + "</span></li>\n" +
        "                        </ul>" +
        " <div style='margin-top: 10px' class=\"btn-group btn-group-justified\" role=\"group\" aria-label=\"Justified button group\">\n" +
        "   <a data-toggle=\"modal\" data-target=\"#updateModal\" productID=\"" + i + "\" class=\"btn btn-success\" href=\"#\" role=\"button\" >Update</a>\n" +
      "   <a   orderID=\"" + orders[i]._id + "\" class=\"btn btn-warning\" href=\"#\" role=\"button\" >Sended</a>\n" +
        "   <a  data-toggle=\"modal\" data-target=\"#deleteOrder\" orderID=\"" + orders[i]._id + "\" class=\"btn btn-danger\" href=\"#\" role=\"button\" onclick='deleteOrderConfirm(this)'>Delete</a>\n" +
        " </div>"  +
        "                    </div>\n" +
        "                </div>").hide().fadeIn(500);

}

function deleteOrderConfirm(ID) {
    deletingOrderID = ID.getAttribute('orderID');
}

function deleteOrder(){

    $.ajax({
        url: `${baseUrl}/orders/${deletingOrderID}`,
        dataType: 'json',
        crossDomain: true,
        type: 'DELETE',
        success: function () {
            dialogAlert("Order was deleted.", "success");
            getOrders();
            closeDetail()
        },
        error: function (req, status, err) {
            dialogAlert("Something went wrong.", "danger");
        }
    });
}

function createOrder() {
    if(JSON.parse(localStorage.getItem('cartOrders')) != null){
        orderCart = JSON.parse(localStorage.getItem('cartOrders'));

        $('#ownerOrder').val(orderCart.owner);
        $('#typeOrder').val(orderCart.type);
        $('#commentOrder').val(orderCart.comment);
        $('#descriptionOrder').val(orderCart.description);
        $('#courierOrder').val(orderCart.courier)
    }



        // "destination": `${}, ${$('#streetDestinationOrder').val()} ${$('#numberStreetDestinationOrder').val()}, ${$('#zipCodeOrder').val()}`,

    $('.productsListModal').empty().append('' +
        '<table class="table formTable">\n' +
        '                <thead class="thead-dark"></thead>\n' +
        '                <tbody></tbody>\n' +
        '              </table>')
    let index = 1;
    $('.formTable > thead').empty();
    $('.formTable > tbody').empty();

    $('.formTable > tbody').hide().fadeIn('slow');
    $('.formTable > thead').empty().append("<tr>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Index</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Name</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Price</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Available</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Quantity</th>\n" +
        "                    <th style='border-right: 1px solid #cbcbcb' scope=\"col\">Add</th>\n" +
        "                </tr>");
    $(".formTable > tbody").empty();
    for (let product of products) {

        $(".formTable > tbody").append("" +
            "<tr class='productRow'>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + index + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb' class='productName'>" + product.name + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.price + " $</td>" +
            "<td style='border-right: 1px solid #cbcbcb'>" + product.available + "</td>" +
            "<td style='border-right: 1px solid #cbcbcb'> <input type=\"number\" placeholder='0' class='form-control' min='0' max='" + product.available + "' id='quantityInp" + (index - 1) + "'></td>" +
            "<td style='border-right: 1px solid #cbcbcb'> <button style='display: block;margin: 0 auto; width: 100%' type='button' id='cartBtn" + (index - 1) + "' class='btn  btn-outline-info' onclick='addToOrderCard(" + (index - 1) + ")'>\<i id=faCls" + (index - 1) + "  class=\"fa fa-plus\" aria-hidden=\"true\"></i>\n</button></td>" +
            "</tr>");

        if(JSON.parse(localStorage.getItem('cartOrders')) != null ){
            orderCart.products.forEach(item =>{
                if(product._id == item._id){
                    $(`#faCls${(index - 1)}`).removeClass('fa-plus').addClass('fa-minus');
                    $(`#quantityInp${(index - 1)}`).val(item.quantity);
                    $(`#cartBtn${(index - 1)}`).removeClass('btn-outline-info').addClass('btn-success');
                }
            })
        }

        index++
    }
}

function addToOrderCard(index) {



    if($(`#quantityInp${index}`).val() === '' || $(`#quantityInp${index}`).val() == 0){
        dialogAlert("Cant add item without quantity", "warning");
    } else {

        if($(`#quantityInp${index}`).val() > products[index].available) {

            $(`#quantityInp${index}`).val(products[index].available);
            dialogAlert(`You cant order more then ${products[index].available}`, "warning");
            return;
        }

        let orderItem = {
            "_id": products[index]._id,
            "quantity":$(`#quantityInp${index}`).val(),
        };


        if($(`#faCls${index}`).hasClass('fa-plus')){
            $(`#faCls${index}`).removeClass('fa-plus').addClass('fa-minus');
            $(`#cartBtn${index}`).removeClass('btn-outline-info').addClass('btn-success');
            orderCart.products.push(orderItem);
            dialogAlert(`Item was added`, "success");
            localStorage.setItem('cartOrders',JSON.stringify(orderCart))
        }else {
            $(`#faCls${index}`).removeClass('fa-minus').addClass('fa-plus');
            $(`#cartBtn${index}`).removeClass('btn-success').addClass('btn-outline-info');

            $.each(orderCart.products, function(i, el){
                if (this._id == products[index]._id){
                    orderCart.products.splice(i, 1);
                }
            });
            dialogAlert(`Item was removed`, "info");
            localStorage.setItem('cartOrders',JSON.stringify(orderCart))
        }
    }
}

function clearOrderCart() {
    orderCart = {
        orderInfo: {},
        orderItems:[]
    };
    localStorage.setItem('cartOrders',null)
}

function formController() {
    orderCart = {
        "owner": $('#ownerOrder').val(),
        "destination": `${$('#cityDestinationOrder').val()}, ${$('#streetDestinationOrder').val()} ${$('#numberStreetDestinationOrder').val()}, ${$('#zipCodeOrder').val()}`,
        "type": $('#typeOrder').val(),
        "createdDate": new Date(),
        "comment": $('#commentOrder').val(),
        "description": $('#descriptionOrder').val(),
        "deliveryPrice": $('#deliveryPriceOrder').val(),
        "courier": $('#courierOrder').val(),
        "deliveryTime": $('#deliveryTimeOrder').val(),
        "payed": false,
        products:orderCart.products
    };
    localStorage.setItem('cartOrders',JSON.stringify(orderCart))
}

function submitOrder() {
    $.ajax({
        url: `${baseUrl}/orders`,
        contentType: 'application/json',
        crossDomain: true,
        data: JSON.stringify(orderCart),
        type: 'POST',
        success: function () {
            dialogAlert("Order was created", "success");
            getProducts();
            $("#createOrder").fadeOut('slow');
            $('.modal-backdrop').fadeOut("slow");

        },
        error: function (request, status, err) {
            dialogAlert(request.responseJSON.msg, "danger");
        }
    });
}
