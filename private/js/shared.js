//Global
let baseUrl = "http://localhost:3000";
let products = [];
let deletingID = "";
let productID;
let orders;
let deletingOrderID;
let orderCart = {
    products:[]
}



function dialogAlert(msg, type) {
    $(".dialogAlert").fadeOut();

    $("body").prepend("<div class=\"alert alert-" + type + " alert-dismissible dialogAlert\" style=\"position: absolute; z-index: 10002; width: 50%; top: 10px; left:0; right:0; margin-left:auto;margin-right:auto;\">\n" +
        "    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n" +
        "    <strong class='capitalize'>" + type + "!</strong> " + msg +
        "</div>").fadeIn('slow');
    setTimeout(function () {
        $(".dialogAlert").fadeOut("slow")
    }, 5000)
}

function updateModal() {

    $('#updateModal').fadeOut("slow");
    $('.modal-backdrop').fadeOut("slow");
    $('#productForm').trigger('reset');

    setTimeout(function () {
        $(".dialogAlert").fadeOut("slow")
    }, 3000)
}

function closeDetail() {
    $("#info").empty()
}

function emptyPage(msg) {
    $('table').hide();
    $('.centerPage').empty();
    $('.centerPage').prepend(
        "<div class=\"alert alert-dark text-center\" role=\"alert\">\n" +
        msg +
        "</div>").hide().fadeIn('slow')
}

function serchOrderModalTable() {

    let i = 0;
    for(let label of $('.productName')){
        if (label.innerHTML.toUpperCase().indexOf($('#searchProduct').val().toUpperCase()) > -1) {
            $('.productRow')[i].style.display = "";
        } else {
            $('.productRow')[i].style.display = "none";
        }
        i++
    }

}