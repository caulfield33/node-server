function login() {
    let userData = {
        email: $("#emailUser").val(),
        password: $("#passwordUser").val()
    };
    $.ajax({
        url: `${baseUrl}/users/login`,
        contentType: 'application/json',
        crossDomain: true,
        data: JSON.stringify(userData),
        type: 'POST',
        success: function (request, status) {
            console.log(request);
            $("body").empty().append(request.user.page);
            setTimeout(getProducts(), 100)

        },
        error: function (request, status, err) {
            dialogAlert(request.responseJSON.msg, "danger");
        }
    });
}