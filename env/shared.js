const mongodbConnection = "mongodb://suAdmin:someCoolPass@cluster0-shard-00-00-hqlvg.mongodb.net:27017,cluster0-shard-00-01-hqlvg.mongodb.net:27017,cluster0-shard-00-02-hqlvg.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";
const baseUrl = "http://localhost:3000";
const jwtKey = "someCoolKey";
const accessControl = "http://localhost:8000";

module.exports = {
    baseUrl: baseUrl,
    mongodbConnection: mongodbConnection,
    jwtKey: jwtKey,
    accessControl: accessControl
};