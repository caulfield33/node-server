const UserController = require('../controllers/usersController');

const express   = require('express');
const router    = express.Router();
const checkAuth = require('../auth/auth');


router.post('/', UserController.createNewUser);

router.post('/login', UserController.userLogin);

router.get('/:userID' , UserController.getSingleUser);

router.get('/', UserController.getAllUser);

router.delete('/:userID', UserController.deleteSingleUser);


module.exports = router;