const StatisticsController = require('../controllers/statisticsController');

const express   = require('express');
const router    = express.Router();

router.get('/orders', StatisticsController.getOrdersInfo);
router.get('/products', StatisticsController.getProductsInfo);

module.exports = router;