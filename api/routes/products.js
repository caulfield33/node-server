const ProductController = require("../controllers/productsController") ;

const express   = require('express');
const multer    = require('multer');
const checkAuth = require('../auth/auth');

//Upload file config
const fileFilter = (request, file, callback) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        callback(null, true);
    } else {
        callback(null, false);
    }
};
const storage = multer.diskStorage({
    destination: function(request, file, callback) {
        callback(null, './private/img/');
    },
    filename: function(request, file, callback) {
        callback(null, new Date().toISOString() + "~" + file.originalname);
    }
});
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
});

const router    = express.Router();

router.get('/', ProductController.getAllProducts);

router.post('/'  , ProductController.createNewProduct); //, upload.single('images')

router.put('/:productID', ProductController.updateProduct );

router.get('/:productID', ProductController.getSingleProduct);

router.delete('/:productID' , ProductController.deleteProduct);

module.exports = router;
