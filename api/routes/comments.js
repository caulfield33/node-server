const CommentController = require('../controllers/commentsController');

const express   = require('express');

const router    = express.Router();


router.post('/:productID', CommentController.createNewProductComment );

router.get('/:commentID', CommentController.getSingleProductComment);

router.get('/product/:productID', CommentController.getAllProductComments);

router.patch('/:commendID', CommentController.updateSingleComment);

router.delete('/:commentID', CommentController.deleteSingleComment);

module.exports = router;