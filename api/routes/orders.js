const OrderController = require('../controllers/ordersController');

const express   = require('express');
const router    = express.Router();
const checkAuth = require('../auth/auth');

router.get('/', OrderController.getAllOrders);

router.post('/' , OrderController.createNewOrder);

router.get('/:orderID', OrderController.getSingleOrder);

router.patch('/:orderID', OrderController.updateSingleOrder);

router.delete('/:orderID', OrderController.deleteSingleOrder);

module.exports = router;