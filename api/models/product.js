const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id         : mongoose.Schema.Types.ObjectId,
    name        : {type: String, required: true},
    description : String,
    price       : {type: Number, required: true},
    createdDate : String,
    owner       : {type: String, required: true},
    available   : {type: Number, required: true},
    images      : String,
    discount    : Number,
    type        : String,
    category    : String,
    vipProduct  : Boolean,
    maxOrder    : {type: Number, required: true},
    views       : Number,
    wish        : Number,
    infoLinks   : String,
    comments    : [{
        comment : {type: mongoose.Schema.Types.ObjectId, ref: 'ProductComment'},
    }],
});

module.exports = mongoose.model('Product', productSchema);
// {
//     "_id": "5ab2343a8513e62fc8fbf64d",
//     "name": "IMG",
//     "description": "IMG Models considers the safety and well-being of aspiring models a top priority. We pride ourselves on our professionalism, transparency, and authenticity and believe it is important to advise models about unscrupulous people who prey on their ambitions.",
//     "price": 99.99,
//     "createdDate": "Wed Mar 21 2018 11:30:18 GMT+0100 (ora solare Europa occidentale)",
//     "owner": "Node Sop",
//     "available": 50,
//     "images": "https://yt3.ggpht.com/a-/AJLlDp2OpaQLSwfYsNVHuzItGpKsW0fTZrDT5HeDlw=s900-mo-c-c0xffffffff-rj-k-no",
//     "discount": 0,
//     "type": "Image",
//     "category": "Photo",
//     "vipProduct": false,
//     "maxOrder": 5,
//     "views": 0,
//     "wish": 0,
//     "infoLinks": "https://it.lipsum.com/",
//     "__v": 0
// },