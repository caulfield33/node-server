const mongoose = require('mongoose');

const productCommentSchema = mongoose.Schema({
    _id           : mongoose.Schema.Types.ObjectId,
    owner         : {type: String, required: true},//TODO create user schema
    createdDate   : {type: String, required: true},
    comment       : String,
    rating        : {type: Number, required: true}
});

module.exports = mongoose.model('ProductComment', productCommentSchema);