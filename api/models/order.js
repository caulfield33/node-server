const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id           : mongoose.Schema.Types.ObjectId,
    owner         : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    destination   : {type: String, required: false},
    type          : {type: String, required: false},
    createdDate   : {type: String, required: false},
    comment       : String,
    description   : String,
    deliveryPrice : {type: Number, required: false},
    courier       : {type: String, required: false},
    deliveryTime  : String,
    payed         : {type: Boolean, required: false},
    products      : [{
        product   : {type: mongoose.Schema.Types.ObjectId, ref: 'Product'},
        quantity  : {type: Number, default: 1}
    }],

});

module.exports = mongoose.model('Order', orderSchema);

// {
//     "owner"         : "request.body.owne",
//     "destination"   : "Some test desc",
//     "type"          : "Ordinary",
//     "comment"       : "Some comments",
//     "description"   : "",
//     "deliveryPrice" : 300,
//     "courier"       : "Amazon",
//     "deliveryTime"  : "Wed Mar 21 2018 11:28:25 GMT+0100 (ora solare Europa occidentale)",
//     "payed"         : "false",
//     "products"      : [
//     {
//         "_id": "5ab2343a8513e62fc8fbf645",
//         "quantity": 5
//     },
//     {
//         "_id": "5ab233c9fb220c1610e66fc5",
//         "quantity": 2
//     },
//     {
//         "_id": "5ab2343a8513e62fc8fbf64f"
//     }]
// }