const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id           : mongoose.Schema.Types.ObjectId,
    firsName      : {type: String, required: true},
    lastName      : {type: String, required: true},
    age           : Number,
    password      : {type: String, required: true},
    email         : {type: String, required: true, unique: true, match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/},
    photo         : String,
    aboutMe       : String,
    address       : String,
    rating        : [Number],
    createdDate   : String,
    lastLogin     : String,
    vipUser       : {type: Boolean, default: false},
    orders        : [{type: mongoose.Schema.Types.ObjectId, ref: 'Order'}],
    comments      : [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],

});

module.exports = mongoose.model('userSchema', userSchema);