const mongoose  = require('mongoose');
const User      = require('../models/user');
const shared    = require('../../env/shared');
const bcrypt    = require('bcrypt');
const jwt       = require('jsonwebtoken');

const baseUrl = shared.baseUrl;

exports.createNewUser    = (request, response) => {

    User.find({email: request.body.email}).exec()
        .then(user => {
            if (user.length >= 1) {
                return response.status(409).json({
                    msg: "User exist."
                })
            } else {
                bcrypt.hash(request.body.password, 10, (error, hash) => {
                    if (error) {
                        return response.status(500).json({
                            error: error
                        })
                    } else {

                        const user = new User({
                            _id         : new mongoose.Types.ObjectId,
                            firsName    : request.body.firsName,
                            lastName    : request.body.lastName,
                            age         : request.body.age,
                            password    : hash,
                            email       : request.body.email,
                            photo       : request.body.photo,
                            aboutMe     : request.body.aboutMe,
                            address     : request.body.address,
                            rating      : [],
                            createdDate : new Date,
                            lastLogin   : new Date,
                            vipUser     : request.body.vipUser,
                            orders      : [],
                            comments    : [],
                        });

                        console.log(user);

                        user.save()
                            .then(result => {
                                const token = jwt.sign({
                                        _id         : result._id,
                                        firsName    : result.firsName,
                                        lastName    : result.lastName,
                                        age         : result.age,
                                        email       : result.email,
                                        photo       : result.photo,
                                        aboutMe     : result.aboutMe,
                                        address     : result.address,
                                        rating      : result.rating,
                                        createdDate : result.createdDate,
                                        lastLogin   : result.lastLogin,
                                        vipUser     : result.vipUser,
                                        orders      : result.orders.length,
                                        comments    : result.comments.length,
                                        page        : shared.homePage
                                    },
                                    shared.jwtKey,
                                    {
                                        expiresIn:"1h"
                                    });

                                response.status(201).json({
                                    msg: 'User created successful.',
                                    responseData: {
                                        method: "POST",
                                        url: `${baseUrl}/users/${result._id}`
                                    },
                                    user: token
                                })
                            })
                            .catch(error => {
                                response.status(500).json({
                                    msg: 'Cant create user',
                                    error: error,
                                })
                            });
                    }
                })
            }
        })

};

exports.userLogin        = (request, response) => {
    User.findOne({email: request.body.email}).exec()
        .then(user => {
            if (user.length < 1) {
                response.status(401).json({
                    msg: `Auth fail`,
                    requestData: {
                        method: "POST",
                    }
                })
            } else {
                bcrypt.compare(request.body.password, user.password, (error , result) =>{
                    if(error){
                        return response.status(401).json({
                            msg: `Auth fail`,
                            requestData: {
                                method: "POST",
                            }
                        })
                    }
                    if(result){
                        const token = jwt.sign({
                                _id         : user._id,
                                firsName    : user.firsName,
                                lastName    : user.lastName,
                                age         : user.age,
                                email       : user.email,
                                photo       : user.photo,
                                aboutMe     : user.aboutMe,
                                address     : user.address,
                                rating      : user.rating,
                                createdDate : user.createdDate,
                                lastLogin   : user.lastLogin,
                                vipUser     : user.vipUser,
                                orders      : user.orders,
                                comments    : user.comments,

                            },
                            shared.jwtKey,
                            {
                                expiresIn:"1h"
                            });

                        return response.status(200).json({
                            msg: `Auth successful`,
                            token: token,
                            user: {
                                _id         : user._id,
                                firsName    : user.firsName,
                                lastName    : user.lastName,
                                age         : user.age,
                                email       : user.email,
                                photo       : user.photo,
                                aboutMe     : user.aboutMe,
                                address     : user.address,
                                rating      : user.rating,
                                createdDate : user.createdDate,
                                lastLogin   : user.lastLogin,
                                vipUser     : user.vipUser,
                                orders      : user.orders,
                                comments    : user.comments,
                                page        : shared.homePage
                            },
                            requestData: {
                                method: "POST",
                            },
                        })

                    }
                })
            }

        })
        .catch(error => {
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });


};

exports.getSingleUser    = (request, response) => {

    const userID = request.params.userID;

    User.findById(userID)
        .populate('orders')
        .populate('orders.products._id')
        .exec()
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `User by ID: ${userID}. Was founded`,
                    requestData: {
                        method: "GET",
                    },
                    result: result
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found by ID: ${userID}`
                })
            }

        })
        .catch(error => {
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.getAllUser       = (request, response) => {

    User.find()
        .populate('orders')
        .populate('orders.products._id')
        .exec()
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `User `,
                    requestData: {
                        method: "GET",
                    },
                    result: result
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found `
                })
            }

        })
        .catch(error => {
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.deleteSingleUser = (request, response) => {

    User.remove({_id: request.param.userID}).exec()
        .then(result =>{
            return result.status(200).json({
                msg: "User was deleted."
            })
        })
        .catch( error =>{
            return response.status(500).json({
                error: error
            })
        })

};