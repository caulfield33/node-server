const Product   = require('../models/product');
const Comment   = require('../models/productComment');
const User      = require('../models/user');
const mongoose  = require('mongoose');
const shared    = require('../../env/shared');

const baseUrl   = shared.baseUrl;

exports.createNewProductComment = (request, response) => {

    const ID = request.params.productID;

    const comment = new Comment({
        _id           : new mongoose.Types.ObjectId,
        owner         : request.body.owner,
        createdDate   : new Date,
        comment       : request.body.comment,
        rating        : request.body.rating
    });
    comment.save()
        .then(result => {
            Product.update({_id: ID}, { "$push": { "comments": comment } })
                .exec()
                .then(() => {
                    User.update({_id: result.owner}, { "$push": { "comments": comment } })
                        .exec()
                        .then(() => {
                            response.status(200).json({
                                msg: 'Comment was added',
                                requestData: {
                                    method: "POST",
                                    url: `${baseUrl}/comments/${result._id}/`,
                                }
                            });
                        })
                        .catch(error => {
                            response.status(404).json({
                                msg: 'Cant create comment',
                                error: error
                            })
                        })
                })
                .catch(error => {
                    response.status(404).json({
                        msg: 'Cant create comment',
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Cant create comment',
                error: error
            })
        });


};

exports.getSingleProductComment = (request, response) => {

    const commentID = request.params.commentID;

    Comment.findById(commentID).exec()
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `Comment by ID: ${commentID}. Was founded`,
                    requestData : {
                        method: "GET",
                    },
                    result: result
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found by ID: ${commentID}`
                })
            }

        })
        .catch(error => {
            console.log(error);
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.getAllProductComments   = (request, response) => {

    const productID = request.params.productID;

    Product.findById(productID)
        .populate('comments._id')
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `Product comments by ID: ${productID}. Was founded`,
                    requestData : {
                        method: "GET",
                    },
                    result: {
                        productID   : result._id,
                        comments    : result.comments,
                        rating      : result.rating,
                        owner       : result.owner,
                        commentData : {
                            method: "GET",
                            url: `${baseUrl}/products/${result._id}`,
                        }
                    }
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found by ID: ${productID}`
                })
            }

        })
        .catch(error => {
            console.log(error);
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.updateSingleComment     = (request, response) => {

    const commendID = request.params.commendID;

    const updateValue = {};

    for (const uVal of request.body) {
        updateValue[uVal.propName] = uVal.value;
    }//TODO creare un oggetto solo con le proprieta da aggiornare
    Comment.update({_id: commendID}, {$set: updateValue}).exec()
        .then(() => {
            response.status(201).json({
                msg: 'Comment was updated',
                requestData: {
                    method: "PATCH",
                    url: `${baseUrl}/comments/${commendID}`,
                }
            })
        })
        .catch(error => {
            response.status(404).json({
                msg: 'Cant update comment',
                error: error
            })
        })
};

exports.deleteSingleComment     = (request, response) => {

    const commentID = request.params.commentID;

    Comment.remove({_id: commentID}).exec()
        .then(() => {
            response.status(201).json({
                msg: `Comment ID: ${commentID}. Was deleted`,
                requestData: {
                    method: "DELETE",
                }
            })
        })
        .catch(error => {
            console.log(error);
            response.status(500).json({
                msg: 'Deleting filed.',
                error: error
            })
        })
};