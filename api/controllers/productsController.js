const Product   = require('../models/product');
const shared    = require('../../env/shared');
const mongoose  = require('mongoose');

const baseUrl   = shared.baseUrl;

exports.getAllProducts      = (request, response) => {

    Product
        .find()
        .populate('comments._id')
        .select('_id name description price createdDate owner available images' +
            ' discount type category vipProduct maxOrder views wish infoLinks comments')
        .exec()
        .then(result => {
            const responseGet = {
                "count": result.length,
                "requestData" : {
                    method: "GET",
                    url: `${baseUrl}/products`,
                },
                "products": result.map(result => {
                    return {
                        _id         : result._id,
                        name        : result.name,
                        description : result.description,
                        price       : result.price,
                        createdDate : result.createdDate,
                        owner       : result.owner,
                        available   : result.available,
                        images      : result.images,
                        discount    : result.discount,
                        type        : result.type,
                        category    : result.category,
                        vipProduct  : result.vipProduct,
                        maxOrder    : result.maxOrder,
                        views       : result.views,
                        wish        : result.wish,
                        infoLinks   : result.infoLinks,
                        comments    : result.comments,
                        productData : {
                            method: "GET",
                            url: `${baseUrl}/products/${result._id}`,
                        }
                    }
                })
            };

            if (result.length !== 0) {
                response.status(200).json(responseGet)
            } else {
                response.status(404).json({
                    msg: 'Cant find any product',
                    result: result
                })
            }
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Some thing wrong with product request',
                error: error
            })
        })
};

exports.createNewProduct    = (request, response) => {

    const product = new Product({
        _id             : new mongoose.Types.ObjectId,
        name            : request.body.name,
        description     : request.body.description,
        price           : request.body.price,
        createdDate     : new Date("dd/MM/yyyy"),
        owner           : request.body.owner,
        available       : request.body.available,
        // images          : "img/"+ request.file.filename || "",
        discount        : request.body.discount         || 0,
        type            : request.body.type             || "",
        category        : request.body.category         || "",
        vipProduct      : request.body.vipProduct       || false,
        maxOrder        : request.body.maxOrder,
        restricted      : request.body.restricted       || "",
        views           : Math.floor((Math.random() * 1000) + 1), //TODO math random devo essere a 0
        wish            : Math.floor((Math.random() * 1000) + 1),
        infoLinks       : request.body.infoLinks        || "",
        comments        : []
    });

    product.save()
        .then(result => {
            response.status(201).json({
                msg: 'Product was created.',
                createdProduct: {
                    _id         : result._id,
                    name        : result.name,
                    description : result.description,
                    price       : result.price,
                    createdDate : result.createdDate,
                    owner       : result.owner,
                    available   : result.available,
                    images      : result.images,
                    discount    : result.discount,
                    type        : result.type,
                    category    : result.category,
                    vipProduct  : result.vipProduct,
                    maxOrder    : result.maxOrder,
                    views       : result.views,
                    wish        : result.wish,
                    infoLinks   : result.infoLinks,
                    comments    : [],
                    requestData : {
                        method: "POST",
                        url: `${baseUrl}/products/${result._id}`,
                    }
                }
            })
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Cant create product',
                error: error
            })
        });

};

exports.updateProduct       = (request, response) => {

    const ID = request.params.productID;

    const updateValue = {};

    console.log(request.body);

    for (const uVal of request.body) {
        updateValue[uVal.propName] = uVal.value; //TODO creare un obgetc solo con i campi da agiornare
    }

    Product.update({_id: ID}, {$set: updateValue}).exec()
        .then(() => {
            response.status(200).json({
                msg: 'Products was updated',
                requestData: {
                    method: "PATCH",
                    url: `${baseUrl}/products/${ID}`,
                }
            })
        })
        .catch(error => {
            response.status(404).json({
                msg: 'Cant update product',
                error: error
            })
        })
};

exports.getSingleProduct    = (request, response) => {

    const ID = request.params.productID;

    Product.findById(ID)
        .populate('comments._id')
        .exec()
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `Product by ID: ${ID}. Was founded`,
                    requestData : {
                        method: "GET",
                    },
                    result: {
                        _id         : result._id,
                        name        : result.name,
                        description : result.description,
                        price       : result.price,
                        createdDate : result.createdDate,
                        owner       : result.owner,
                        available   : result.available,
                        images      : result.images,
                        discount    : result.discount,
                        type        : result.type,
                        category    : result.category,
                        vipProduct  : result.vipProduct,
                        maxOrder    : result.maxOrder,
                        views       : result.views,
                        wish        : result.wish,
                        infoLinks   : result.infoLinks,
                        comments    : result.comments,
                        productData : {
                            method: "GET",
                            url: `${baseUrl}/products/${result._id}`,
                        }
                    }
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found by ID: ${ID}`
                })
            }

        })
        .catch(error => {
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.deleteProduct       = (request, response) => {

    const ID = request.params.productID;
    console.log(ID)
    Product.remove({_id: ID}).exec()
        .then(() => {
            response.status(200).json({
                msg: `Product ID: ${ID}. Was deleted`,
                requestData: {
                    method: "DELETE",
                }
            })
        })
        .catch(error => {
            console.log(error);
            response.status(500).json({
                msg: 'Deleting filed.',
                error: error
            })
        })
};