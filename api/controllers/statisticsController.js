const Order     = require('../models/order');
const Product   = require('../models/product');
const shared    = require('../../env/shared');
const baseUrl   = shared.baseUrl;

exports.getOrdersInfo      = (request, response) =>{
    Order.find()
        .exec()
        .then(result => {
            const responseGet = {
                "count": result.length,
                "requestData" : {
                    method: "GET",
                    url: `${baseUrl}/statistic/orders`,
                },
                "orders": result.map(res =>{
                    return{
                        _id           : res._id,
                        type          : res.type,
                        deliveryPrice : res.deliveryPrice,
                        courier       : res.courier,
                        payed         : res.payed,
                    }
                })
            };
            if (result.length !== 0) {
                response.status(201).json(responseGet)
            } else {
                response.status(404).json({
                    msg: 'Cant find any orders',
                    result: result
                })
            }
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Some thing wrong with order request',
                error: error
            })
        })
};

exports.getProductsInfo      = (request, response) => {

    Product
        .find()
        .exec()
        .then(result => {
            const responseGet = {
                "count": result.length,
                "requestData" : {
                    method: "GET",
                    url: `${baseUrl}/statistics/products`,
                },
                "products": result.map(result => {
                    return {
                        _id         : result._id,
                        price       : result.price,
                        createdDate : result.createdDate,
                        owner       : result.owner,
                        available   : result.available,
                        discount    : result.discount,
                        type        : result.type,
                        category    : result.category,
                        vipProduct  : result.vipProduct,
                        views       : result.views,
                        wish        : result.wish,
                        productData : {
                            method: "GET",
                            url: `${baseUrl}/products/${result._id}`,
                        }
                    }
                })
            };

            if (result.length !== 0) {
                response.status(200).json(responseGet)
            } else {
                response.status(404).json({
                    msg: 'Cant find any product',
                    result: result
                })
            }
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Some thing wrong with product request',
                error: error
            })
        })
};