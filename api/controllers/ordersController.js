const User      = require('../models/user');
const Order     = require('../models/order');
const shared    = require('../../env/shared');
const mongoose  = require('mongoose');

const baseUrl   = shared.baseUrl;


exports.getAllOrders      = (request, response) =>{

    Order.find()
        .populate({  path: 'products._id', model: 'Product',})
        .exec()
        .then(result => {
            const responseGet = {
                "count": result.length,
                "requestData" : {
                    method: "GET",
                    url: `${baseUrl}/orders`,
                },
                "orders": result.map(res =>{
                    return{
                        _id           : res._id,
                        owner         : res.owner,
                        destination   : res.destination,
                        type          : res.type,
                        createdDate   : res.createdDate,
                        comment       : res.comment,
                        description   : res.destination,
                        deliveryPrice : res.deliveryPrice,
                        courier       : res.courier,
                        deliveryTime  : res.deliveryTime,
                        payed         : res.payed,
                        products      : res.products,
                    }
                })
            };
            if (result.length !== 0) {
                response.status(201).json(responseGet)
            } else {
                response.status(404).json({
                    msg: 'Cant find any orders',
                    result: result
                })
            }
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Some thing wrong with order request',
                error: error
            })
        })
};

exports.createNewOrder    = (request, response) =>{
    console.log(request.body);

    const order = new Order({
        _id           : new mongoose.Types.ObjectId,
        owner         : new mongoose.Types.ObjectId,
        destination   : request.body.destination,
        type          : request.body.type,
        createdDate   : new Date,
        comment       : request.body.comment        || "",
        description   : request.body.description    || "",
        deliveryPrice : request.body.deliveryPrice,
        courier       : request.body.courier,
        deliveryTime  : request.body.deliveryTime   || "Undefined",
        payed         : request.body.payed,
        products      : request.body.products,
    });

    order.save()
        .then( result => {
            User.update({_id: result.owner}, { "$push": { "orders": order } })
                .exec()
                .then(() => {
                    response.status(201).json({
                        msg: 'Order was added',
                        requestData: {
                            method: "POST",
                            url: `${baseUrl}/orders/${result._id}/`,
                        }
                    });
                    console.log(result)
                })
                .catch(error => {
                    response.status(404).json({
                        msg: 'Cant create order',
                        error: error
                    })
                })
        })
        .catch(error => {
            console.log(error)
            response.status(500).json({
                msg: 'Cant create order',
                error: error,
            })
        });

};

exports.getSingleOrder    = (request, response) =>{

    const ID = request.params.orderID;

    Order.findById(ID)
        .populate({  path: 'products._id', model: 'Product' })
        .exec()
        .then(result => {
            if (result) {
                response.status(200).json({
                    msg: `Order by ID: ${ID}. Was founded`,
                    requestData : {
                        method: "GET",
                    },
                    result: result
                })
            } else {
                response.status(404).json({
                    msg: `Nothing found by ID: ${ID}`
                })
            }

        })
        .catch(error => {
            response.status(500).json({
                msg: 'Searching filed.',
                error: error
            })
        });
};

exports.updateSingleOrder = (request, response) => {

    const orderID = request.params.orderID;

    const updateValue = {};

    for (const uVal of request.body) {
        updateValue[uVal.propName] = uVal.value; //per non cambiare i valori che non sono stati passati
    }
    //TODO creare un oggetto con i campi da oggiornare

    Order.update({_id: orderID}, {$set: updateValue}).exec()
        .then(() => {
            response.status(200).json({
                msg: 'Order was updated',
                requestData: {
                    method: "PATCH",
                    url: `${baseUrl}/products/${orderID}`,
                }
            })
        })
        .catch(error => {
            response.status(404).json({
                msg: 'Cant update product',
                error: error
            })
        })
};

exports.deleteSingleOrder = (request, response) => {

    const ID = request.params.orderID;

    Order.remove({_id: ID}).exec()
        .then(() => {
            response.status(200).json({
                msg: `Order ID: ${ID}. Was deleted`,
                requestData: {
                    method: "DELETE",
                }
            })
        })
        .catch(error => {
            response.status(500).json({
                msg: 'Deleting filed.',
                error: error
            })
        })
};
