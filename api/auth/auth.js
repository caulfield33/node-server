const jwt = require('jsonwebtoken');
const shared = require('../../env/shared');

const jwtKey = shared.jwtKey;

module.exports = (request, response, next) => {
    try {
        const token = request.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, jwtKey);
        request.userData = decoded;
        next();
    } catch (error) {
        return response.status(401).json({
            msg: 'Auth fail'
        });
    }
};
